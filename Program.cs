
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using MyWebApi;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseNpgsql(builder.Configuration.GetConnectionString("DefaultConnection"));
});

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo()
    {
        Version = "v1",
        Title = "Post API",
        Description = "A webapi sample for post items",
        TermsOfService = new Uri("https://www.example.com/terms"),
        Contact = new OpenApiContact()
        {
            Name = "Mr Sarana Bunnag",
            Url = new Uri("https://www.facebook.com/chainsarana")
        },
        License = new OpenApiLicense()
        {
            Name = "..........",
            Url = new Uri("https://www.example.com/licensing")
        }
    });
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFile));
});

var apiConfiguration = builder.Configuration.GetSection("AppSettings:ApiConfiguration").Get<ApiConfiguration>();
builder.Services.AddSingleton(apiConfiguration);

var app = builder.Build();

// Configure the HTTP request pipeline.
//if (app.Environment.IsDevelopment())
//{
    app.UseSwagger(options =>
    {
        options.RouteTemplate = "plan/{documentName}/swagger.json";
    });
    app.UseSwaggerUI(options=>
    {
        options.SwaggerEndpoint("/plan/v1/swagger.json", "v1");
        options.RoutePrefix = "plan";
    });
//}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();
using (var scope = app.Services.CreateScope())
{
    var dataContext = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
    //dataContext.Database.EnsureCreated();
    //dataContext.Database.EnsureDeleted();
    await dataContext.Database.MigrateAsync();
}
app.Run();
